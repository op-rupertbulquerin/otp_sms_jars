# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* This repository contains jar files which is required for our OTP SMS Messenger App (OutboundMarketPlaceMessenger.java)

### How do I get set up? ###

* **Summary of set up**

1. Just copy the **otpSMS** folder and paste it inside the root directory of **ef3_libray** folder.

2. Pull the updated **.classpath** file under **OpenPortDataLoader** project.

* **Manual Setup of classpath**

* Copy the code below and paste it inside your .classpath file

	<classpathentry kind="var" path="JAR_PATH/otpSMS/java-json.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/gson-2.8.0.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/httpcore-4.4.1.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/apache-httpcomponents-httpcore.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/httpasyncclient-4.0-beta4.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/httpcore-nio-4.3.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/httpclient-4.5.jar"/>
	
	<classpathentry kind="var" path="JAR_PATH/otpSMS/unirest-java-1.4.9.jar"/>

* Check the build path for errors: Right Click **OpenPortDataLoader** project -> Build path -> Configure Build path -> Click Libraries tab

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact